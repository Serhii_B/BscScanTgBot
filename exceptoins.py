class HTTPStatus:
    OK = 200
    CREATED = 201
    BAD_REQUEST = 400
    UNAUTHORIZED = 401
    FORBIDDEN = 403
    NOT_FOUND = 404
    INTERNAL_SERVER_ERROR = 500


class BscScanBaseException(Exception):
    def __init__(self, message=None, status_code=None):
        super().__init__(message)
        self.status_code = status_code
        self.message = message

    def __str__(self):
        return f"Exception with status code {self.status_code}\n" \
               f" Message: {self.message}"


class PartnerException(BscScanBaseException):
    def __init__(self, message=None):
        status_code = HTTPStatus.BAD_REQUEST
        message = 'An error occurred with a partner integration' \
            if not message else message
        super().__init__(status_code=status_code, message=message)


class UnexpectedError(BscScanBaseException):
    def __init__(self, message=None):
        status_code = HTTPStatus.BAD_REQUEST
        message = 'Unexpected response occured.' if not message else message
        super().__init__(status_code=status_code, message=message)
