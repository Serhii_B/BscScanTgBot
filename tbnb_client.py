import datetime
import logging

from exceptoins import PartnerException
from utils import conduct_request, convert_from_wei_to_bnb
from settings import BscScan_API_Key_Token, MY_METAMASK_BSC_TESTNET_ADDRESS

logger = logging.getLogger(__name__)


class BscScanClient:
    params = {
        'apikey': BscScan_API_Key_Token
    }

    def __init__(self, address=MY_METAMASK_BSC_TESTNET_ADDRESS):
        self.address = address
        self.transaction_list = list()

    def __call__(self, *args, **kwargs):
        self.params.update({'address': self.address})
        failed_call = self.get_transactions_data_from_address()
        if failed_call:
            return failed_call
        return self.form_data()

    def get_transactions_data_from_address(self):
        params = dict(self.params)
        params.update({'module': 'account', 'action': 'txlist', 'sort': 'asc'})
        try:
            response = conduct_request(request_method='GET', params=params)
        except Exception as e:
            logger.exception(e)
            raise PartnerException(message=e)

        if bool(int(response['status'])):
            [self.transaction_list.append(transaction)
             for transaction in response['result']]
        else:
            logger.error(f"Error occured with address: {self.address}, and"
                         f" response:  {response['message']}")
            return response

    def form_data(self):
        result = {self.address: list()}
        transactions = list(self.transaction_list)

        for transaction in transactions:
            _datetime = datetime.datetime.fromtimestamp(
                int(transaction['timeStamp'])
            )

            result[self.address].append({
                "date": _datetime.date().isoformat(),
                "time": _datetime.time().isoformat(),
                "transaction_type": transaction['input'],
                "amount": convert_from_wei_to_bnb(int(transaction['value'])),
                "crypto": "BTN",
                "transaction_status": "Success" if bool(transaction['txreceipt_status']) else "Fail",
                "transaction_hash": transaction['hash']
            })

        return result
