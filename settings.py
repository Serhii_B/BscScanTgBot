import os
from dotenv import load_dotenv

load_dotenv()


BscScan_API_Key_Token = os.environ.get('BscScan_API_Key_Token')
Telegram_HTTP_API_TOKEN = os.environ.get('Telegram_HTTP_API_TOKEN')
BscScan_URL_start = os.environ.get('BscScan_URL_start')
BscScan_Testnet_URL_start = os.environ.get('BscScan_Testnet_URL_start')
MY_METAMASK_BSC_TESTNET_ADDRESS = os.environ.get('MY_METAMASK_BSC_TESTNET_ADDRESS')
