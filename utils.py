import requests
from settings import BscScan_Testnet_URL_start


def conduct_request(request_method, params, url=BscScan_Testnet_URL_start):
    response = requests.get(url=url, params=params)
    if response.status_code == 200:
        response = response.json()
    return response


def convert_from_wei_to_bnb(wei_int):
    return int(wei_int)/10**18

