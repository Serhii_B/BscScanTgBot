import logging
import time

from telegram import Update
from telegram.error import RetryAfter
from telegram.ext import (
    ApplicationBuilder,
    ContextTypes,
    CommandHandler,
    MessageHandler,
    filters
)

from exceptoins import PartnerException
from settings import Telegram_HTTP_API_TOKEN
from tbnb_client import BscScanClient

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

supported_command = [
    '/start',
    '/help',
    '/default_address_stats',
    '/get_specific_address_stats'
]


def form_tg_response_data(data: dict):
    for address, transactions in data.items():
        message = f"Transactions of address {address}\n"

        for i, transaction in enumerate(transactions, start=1):
            message += f"\nTransaction {i}:\n"
            message += f"Date: {transaction['date']}\n"
            message += f"Time: {transaction['time']}\n"
            message += f"Transaction Type: {transaction['transaction_type']}\n"
            message += f"Amount: {transaction['amount']} {transaction['crypto']}\n"
            message += f"Transaction Status: {transaction['transaction_status']}\n"
            message += f"Transaction Hash: {transaction['transaction_hash']}\n"

    if len(message) > 4096:
        large_message = [message[j:j+4096] for j in range(0, len(message), 4096)]
        return large_message
    return message


async def _help(update: Update, context: ContextTypes.DEFAULT_TYPE):
    text = "Short info of this bot functionality.\n" \
           "It allows you to check incoming transactions status on BscScan trading platform." \
           "You can check transactions by providing your address or transactions of the default address." \
           "Please type any message to see a list of supported commands."
    await context.bot.send_message(
        chat_id=update.effective_chat.id, text=text
    )


async def unsupported_message(update: Update, context: ContextTypes.DEFAULT_TYPE):
    supported_commands_text = "\n".join(supported_command)
    response_message = f"Unrecognized text. Supported commands are: \n {supported_commands_text}"
    await context.bot.send_message(chat_id=update.effective_chat.id, text=response_message)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id, text="Bot on the wire!"
    )


async def send_chunks(context: ContextTypes.DEFAULT_TYPE, chat_id, message):
    for i, message_chunk in enumerate(message):
        try:
            await context.bot.send_message(chat_id=chat_id, text=message_chunk)
        except RetryAfter as e:
            logging.error(e)
            time.sleep(e.retry_after)
            continue
        except Exception as e:
            logging.error(e)
            raise PartnerException(message=e)


async def default_address_stats(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user_id = update.effective_user.id
    data = BscScanClient()()
    if data.get('status') and not bool(int(data['status'])):
        partner_response = data['result'] if data['result'] else data['message']
        text = f"{partner_response}. Please make sure you are using bot correctly!"
        await context.bot.send_message(chat_id=user_id, text=text)
        raise PartnerException(message=partner_response)
    else:
        message = form_tg_response_data(data)
        if isinstance(message, list):
            await send_chunks(context=context, chat_id=user_id, message=message)

        await context.bot.send_message(chat_id=user_id, text=message)


async def get_specific_address_stats(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user_id = update.effective_user.id

    if context.args:
        address = context.args[0]
        data = BscScanClient(address=address)()

        if data.get('status') and not bool(int(data.get('status'))):
            partner_response = data['result'] if data['result'] else data['message']
            text = f"{partner_response}. Please make sure you are using bot correctly!"
            await context.bot.send_message(chat_id=user_id, text=text)
            raise PartnerException(message=partner_response)
        else:
            message = form_tg_response_data(data)
            if isinstance(message, list):
                await send_chunks(context=context, chat_id=user_id, message=message)

            await context.bot.send_message(chat_id=user_id, text=message)

    else:
        await context.bot.send_message(chat_id=user_id, text="No address was provided ")


def main():
    application = ApplicationBuilder().token(Telegram_HTTP_API_TOKEN).build()

    unsupported_message_handler = MessageHandler(filters.TEXT & (~filters.COMMAND), unsupported_message)
    application.add_handler(unsupported_message_handler)

    start_handler = CommandHandler('start', start)
    application.add_handler(start_handler)

    get_stats_handler = CommandHandler('default_address_stats', default_address_stats)
    application.add_handler(get_stats_handler)

    get_specific_address_stats_handler = CommandHandler('get_specific_address_stats', get_specific_address_stats)
    application.add_handler(get_specific_address_stats_handler)

    help_handler = CommandHandler('help', _help)
    application.add_handler(help_handler)

    application.run_polling()
